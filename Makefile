build:
	flatpak-builder eobcanka cz.eidentita.eobcanka.json --force-clean

run-cardman:
	flatpak-builder --run  eobcanka cz.eidentita.eobcanka.json /app/opt/eObcanka/SpravceKarty/eopcardman.sh

run-authapp:

	flatpak-builder --run  eobcanka cz.eidentita.eobcanka.json /app/opt/eObcanka/Identifikace/eopauthapp.sh

shell:
	flatpak-builder --run  eobcanka cz.eidentita.eobcanka.json /bin/sh
